![Build Status](https://gitlab.com/mcva/mkdocs/badges/master/pipeline.svg)

# mkdocs

[View here](https://mcva.gitlab.io/mkdocs/)

## Local testing

`mkdocs serve`